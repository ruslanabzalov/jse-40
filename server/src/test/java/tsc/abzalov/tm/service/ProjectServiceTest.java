package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.User;

import static org.junit.jupiter.api.Assertions.*;

final class ProjectServiceTest {

    @NotNull
    private static final String LOGIN = "Login";

    @NotNull
    private static final String PASSWORD = "Password";

    @NotNull
    private static final String FIRSTNAME = "Firstname";

    @NotNull
    private static final String EMAIL = "Email";

    @NotNull
    private static final String PROJECT_NAME = "Project Name";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "Project Description";

    @NotNull
    private static final String NEW_PROJECT_NAME = "New Project Name";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "New Project Description";

    @NotNull
    private final IApplicationPropertyService applicationPropertyService = new ApplicationPropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(applicationPropertyService);

    @NotNull
    private final AbstractBusinessEntityService<Project> projectService = new ProjectService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(this.connectionService, this.applicationPropertyService);

    @NotNull
    private final User user = new User();

    @NotNull
    private final Long userId = user.getId();

    @NotNull
    private final Project project = new Project();

    {
        this.userService.clear();
        this.user.setLogin(LOGIN);
        this.user.setPassword(PASSWORD);
        this.user.setFirstname(FIRSTNAME);
        this.user.setEmail(EMAIL);

        this.project.setName(PROJECT_NAME);
        this.project.setDescription(PROJECT_DESCRIPTION);
        this.project.setUserId(this.userId);
    }

    @BeforeEach
    void setUp() {
        userService.clear();
        userService.create(this.user);
        projectService.clear();
        projectService.create(this.project);
    }

    @AfterEach
    void tearDown() {
        projectService.clear();
        userService.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Size Test")
    void size() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.size(null)
                ),
                () -> {
                    projectService.create(this.project);
                    assertEquals(1, projectService.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Empty Test")
    void isEmpty() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.isEmpty(null)
                ),
                () -> assertFalse(projectService.isEmpty())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find All Test")
    void findAll() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.findAll(null)
                ),
                () -> assertEquals(1, projectService.findAll(this.userId).size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Id Test")
    void findById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.findById(null)
                ),
                () -> assertNotNull(projectService.findById(this.project.getId()))
        );
    }

    // TODO: Изменить название аргумента index на position.
//    @Test
//    @Tag("Unit")
//    @DisplayName("Find By Index Test")
//    void findByIndex() {
//        assertAll(
//                () -> assertThrows(
//                        EmptyIdException.class,
//                        () -> projectService.findByIndex(null, 1)
//                ),
//                () -> assertThrows(
//                        IncorrectIndexException.class,
//                        () -> projectService.findByIndex(this.userId, 2)
//                ),
//                () -> assertNotNull(projectService.findByIndex(this.userId, 1))
//        );
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Find By Name Test")
//    void findByName() {
//        assertAll(
//                () -> assertThrows(
//                        EmptyIdException.class,
//                        () -> projectService.findByName(null, PROJECT_NAME)
//                ),
//                () -> assertThrows(
//                        EmptyNameException.class,
//                        () -> projectService.findByName(this.userId, null)
//                ),
//                () -> assertNotNull(projectService.findByName(this.userId, PROJECT_NAME))
//        );
//    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Id Test")
    void editById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.editById(null, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> projectService.editById(this.userId, null, NEW_PROJECT_DESCRIPTION)
                ),
                () -> assertNotNull(projectService.editById(this.project.getId(), NEW_PROJECT_NAME, null))
        );
    }

//    @Test
//    @Tag("Unit")
//    @DisplayName("Edit By Name Test")
//    void editByName() {
//        assertAll(
//                () -> assertThrows(
//                        EmptyIdException.class,
//                        () -> projectService.editByName(null, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION)
//                ),
//                () -> assertThrows(
//                        EmptyNameException.class,
//                        () -> projectService.editByName(this.userId, null, NEW_PROJECT_DESCRIPTION)
//                ),
//                () -> assertNotNull(projectService.editByName(this.userId, PROJECT_NAME, NEW_PROJECT_DESCRIPTION))
//        );
//    }

    @Test
    @Tag("Unit")
    @DisplayName("Clear Test")
    void clear() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.clear(null)
                ),
                () -> {
                    projectService.clear(this.userId);
                    assertTrue(projectService.isEmpty(this.userId));
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Id Test")
    void removeById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.removeById(null)
                ),
                () -> {
                    projectService.removeById(this.project.getId());
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> projectService.findById(this.project.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Name Test")
    void removeByName() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.removeByName(null, PROJECT_NAME)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> projectService.removeByName(this.userId, null)
                ),
                () -> {
                    projectService.removeByName(this.userId, PROJECT_NAME);
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> projectService.findById(this.project.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Start By Id Test")
    void startById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.startById(null)
                ),
                () -> assertNotNull(projectService.startById(this.project.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("End By Id Test")
    void endById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.endById(null)
                ),
                () -> assertNotNull(projectService.endById(this.project.getId()))
        );
    }

}