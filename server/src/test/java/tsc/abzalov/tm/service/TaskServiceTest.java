package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;

import static org.junit.jupiter.api.Assertions.*;

final class TaskServiceTest {

    @NotNull
    private static final String LOGIN = "Login";

    @NotNull
    private static final String PASSWORD = "Password";

    @NotNull
    private static final String FIRSTNAME = "Firstname";

    @NotNull
    private static final String EMAIL = "Email";

    @NotNull
    private static final String TASK_NAME = "Task Name";

    @NotNull
    private static final String TASK_DESCRIPTION = "Task Description";

    @NotNull
    private static final String NEW_TASK_NAME = "New Task Name";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "New Task Description";

    @NotNull
    private final IApplicationPropertyService applicationPropertyService = new ApplicationPropertyService();
    
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(applicationPropertyService);
    
    @NotNull
    private final AbstractBusinessEntityService<Task> taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(this.connectionService, this.applicationPropertyService);

    @NotNull
    private final User user = new User();

    @NotNull
    private final Long userId = user.getId();

    @NotNull
    private final Task task = new Task();

    {
        this.userService.clear();
        this.user.setLogin(LOGIN);
        this.user.setPassword(PASSWORD);
        this.user.setFirstname(FIRSTNAME);
        this.user.setEmail(EMAIL);

        this.task.setName(TASK_NAME);
        this.task.setDescription(TASK_DESCRIPTION);
        this.task.setUserId(this.userId);
    }

    @BeforeEach
    void setUp() {
        userService.clear();
        userService.create(this.user);
        taskService.clear();
        taskService.create(this.task);
    }

    @AfterEach
    void tearDown() {
        taskService.clear();
        userService.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Size Test")
    void size() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.size(null)
                ),
                () -> {
                    taskService.create(this.task);
                    assertEquals(1, taskService.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Empty Test")
    void isEmpty() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.isEmpty(null)
                ),
                () -> assertFalse(taskService.isEmpty())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find All Test")
    void findAll() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findAll(null)
                ),
                () -> assertEquals(1, taskService.findAll(this.userId).size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Id Test")
    void findById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findById(null)
                ),
                () -> assertNotNull(taskService.findById(this.task.getId()))
        );
    }

    // TODO: Изменить название аргумента index на position.
//    @Test
//    @Tag("Unit")
//    @DisplayName("Find By Index Test")
//    void findByIndex() {
//        assertAll(
//                () -> assertThrows(
//                        EmptyIdException.class,
//                        () -> taskService.findByIndex(null, 1)
//                ),
//                () -> assertThrows(
//                        IncorrectIndexException.class,
//                        () -> taskService.findByIndex(this.userId, 2)
//                ),
//                () -> assertNotNull(taskService.findByIndex(this.userId, 1))
//        );
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Find By Name Test")
//    void findByName() {
//        assertAll(
//                () -> assertThrows(
//                        EmptyIdException.class,
//                        () -> taskService.findByName(null, TASK_NAME)
//                ),
//                () -> assertThrows(
//                        EmptyNameException.class,
//                        () -> taskService.findByName(this.userId, null)
//                ),
//                () -> assertNotNull(taskService.findByName(this.userId, TASK_NAME))
//        );
//    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Id Test")
    void editById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.editById(null, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.editById(this.userId, null, NEW_TASK_DESCRIPTION)
                ),
                () -> assertNotNull(taskService.editById(this.task.getId(), NEW_TASK_NAME, null))
        );
    }

//    @Test
//    @Tag("Unit")
//    @DisplayName("Edit By Name Test")
//    void editByName() {
//        assertAll(
//                () -> assertThrows(
//                        EmptyIdException.class,
//                        () -> taskService.editByName(null, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
//                ),
//                () -> assertThrows(
//                        EmptyNameException.class,
//                        () -> taskService.editByName(this.userId, null, NEW_TASK_DESCRIPTION)
//                ),
//                () -> assertNotNull(taskService.editByName(this.userId, TASK_NAME, NEW_TASK_DESCRIPTION))
//        );
//    }

    @Test
    @Tag("Unit")
    @DisplayName("Clear Test")
    void clear() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.clear(null)
                ),
                () -> {
                    taskService.clear(this.userId);
                    assertTrue(taskService.isEmpty(this.userId));
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Id Test")
    void removeById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.removeById(null)
                ),
                () -> {
                    taskService.removeById(this.task.getId());
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> taskService.findById(this.task.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Name Test")
    void removeByName() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.removeByName(null, TASK_NAME)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.removeByName(this.userId, null)
                ),
                () -> {
                    taskService.removeByName(this.userId, TASK_NAME);
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> taskService.findById(this.task.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Start By Id Test")
    void startById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.startById(null)
                ),
                () -> assertNotNull(taskService.startById(this.task.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("End By Id Test")
    void endById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.endById(null)
                ),
                () -> assertNotNull(taskService.endById(this.task.getId()))
        );
    }

}