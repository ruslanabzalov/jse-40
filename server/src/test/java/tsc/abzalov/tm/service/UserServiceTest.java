package tsc.abzalov.tm.service;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.EmptyEmailException;
import tsc.abzalov.tm.exception.auth.EmptyFirstNameException;
import tsc.abzalov.tm.exception.auth.EmptyLoginException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.User;

import static org.junit.jupiter.api.Assertions.*;
import static tsc.abzalov.tm.enumeration.Role.USER;

final class UserServiceTest {

    @NotNull
    private static final String LOGIN = "Login";

    @NotNull
    private static final String PASSWORD = "Password";

    @NotNull
    private static final Role ROLE = USER;

    @NotNull
    private static final String FIRSTNAME = "Firstname";

    @NotNull
    private static final String LASTNAME = "Lastname";

    @NotNull
    private static final String EMAIL = "Email";

    @NotNull
    private final IApplicationPropertyService propertyService = new ApplicationPropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(this.connectionService, this.propertyService);

    @NotNull
    private final User user = new User();

    @NotNull
    private final Long userId = this.user.getId();

    {
        this.userService.clear();
        this.user.setLogin(LOGIN);
        this.user.setPassword(PASSWORD);
        this.user.setFirstname(FIRSTNAME);
        this.user.setEmail(EMAIL);
    }

    @BeforeEach
    void setUp() {
        userService.clear();
        userService.create(user);
    }

    @AfterEach
    void tearDown() {
        userService.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Create Test")
    void create() {
        assertAll(
                () -> assertThrows(
                        IncorrectCredentialsException.class,
                        () -> userService.create(null, PASSWORD, ROLE, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        IncorrectCredentialsException.class,
                        () -> userService.create(LOGIN, null, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        EmptyFirstNameException.class,
                        () -> userService.create(LOGIN, PASSWORD, ROLE, null, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        EmptyEmailException.class,
                        () -> userService.create(LOGIN, PASSWORD, FIRSTNAME, LASTNAME, null)
                ),
                () -> {
                    userService.create(LOGIN, PASSWORD, ROLE, FIRSTNAME, LASTNAME, EMAIL);
                    assertEquals(2, userService.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("User Exist Test")
    void isUserExist() {
        assertAll(
                () -> assertThrows(
                        EmptyLoginException.class,
                        () -> userService.isUserExist(null, EMAIL)
                ),
                () -> assertThrows(
                        EmptyEmailException.class,
                        () -> userService.isUserExist(LOGIN, null)
                ),
                () -> assertFalse(userService.isUserExist(LOGIN + 1, EMAIL)),
                () -> assertFalse(userService.isUserExist(LOGIN, EMAIL + 1)),
                () -> assertTrue(userService.isUserExist(LOGIN, EMAIL))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Login Test")
    void findByLogin() {
        assertAll(
                () -> assertThrows(
                        EmptyLoginException.class,
                        () -> userService.findByLogin(null)
                ),
                () -> assertThrows(
                        EntityNotFoundException.class,
                        () -> userService.findByLogin(LOGIN + 1)
                ),
                () -> {
                    @Nullable val foundedUser = userService.findByLogin(LOGIN);
                    assertEquals(this.user, foundedUser);
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit Password By Id Test")
    void editPasswordById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> userService.editPasswordById(null, PASSWORD + PASSWORD)
                ),
                () -> assertThrows(
                        IncorrectCredentialsException.class,
                        () -> userService.editPasswordById(this.userId, null)
                ),
                () -> assertThrows(
                        EntityNotFoundException.class,
                        () -> userService.editPasswordById(this.userId + 1, PASSWORD + PASSWORD)
                ),
                () -> {
                    @Nullable val updatedUser = userService.editPasswordById(this.userId, PASSWORD + PASSWORD);
                    assertNotNull(updatedUser);
                    assertNotEquals(this.user, updatedUser);
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit User Info Test")
    void editUserInfoById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> userService.editUserInfoById(null, FIRSTNAME + FIRSTNAME, LASTNAME + LASTNAME)
                ),
                () -> assertThrows(
                        EmptyFirstNameException.class,
                        () -> userService.editUserInfoById(this.userId, null, LASTNAME + LASTNAME)
                ),
                () -> assertThrows(
                        EntityNotFoundException.class,
                        () -> userService.editUserInfoById(this.userId + 1, FIRSTNAME + FIRSTNAME, LASTNAME + LASTNAME)
                ),
                () -> {
                    @Nullable val updatedUser = userService.editUserInfoById(this.userId, FIRSTNAME + FIRSTNAME, LASTNAME + LASTNAME);
                    assertNotNull(updatedUser);
                    assertNotEquals(this.user.getFirstname(), updatedUser.getFirstname());
                    assertNotEquals(this.user.getLastname(), updatedUser.getLastname());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Delete By Login Test")
    void deleteByLogin() {
        assertAll(
                () -> assertThrows(
                        EmptyLoginException.class,
                        () -> userService.deleteByLogin(null)
                ),
                () -> {
                    userService.deleteByLogin(LOGIN);
                    assertTrue(userService.isEmpty());
                }
        );
    }

}