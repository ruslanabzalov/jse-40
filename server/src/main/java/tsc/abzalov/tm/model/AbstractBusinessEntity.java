package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Status;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Status.TODO;
import static tsc.abzalov.tm.util.Formatter.DATE_TIME_FORMATTER;
import static tsc.abzalov.tm.util.LiteralConst.*;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractBusinessEntity extends AbstractEntity implements Serializable {
    
    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = TODO;

    @Nullable
    private LocalDateTime startDate;

    @Nullable
    private LocalDateTime endDate;

    @Nullable
    private Long userId;

    @Override
    @NotNull
    public String toString() {
        @NotNull val correctName = Optional.ofNullable(name).orElse(DEFAULT_NAME);
        @NotNull val correctDescription = Optional.ofNullable(description).orElse(DEFAULT_DESCRIPTION);
        @NotNull val correctStartDate = (startDate == null)
                ? IS_NOT_STARTED
                : startDate.format(DATE_TIME_FORMATTER);
        @NotNull val correctEndDate = (endDate == null)
                ? IS_NOT_ENDED
                : endDate.format(DATE_TIME_FORMATTER);

        return correctName +
                ": [ID: " + getId() +
                "; Description: " + correctDescription +
                "; Status: " + this.status.getStatusName() +
                "; Start Date: " + correctStartDate +
                "; End Date: " + correctEndDate +
                "; User ID: " + this.userId + "]";
    }

}

