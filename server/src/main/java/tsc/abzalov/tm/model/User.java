package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import java.io.Serializable;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.LiteralConst.*;

@Data
@EqualsAndHashCode(callSuper = true)
public final class User extends AbstractEntity implements Serializable, Cloneable {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @NotNull
    private Role role = USER;

    @Nullable
    private String firstname;

    @Nullable
    private String lastname;

    @Nullable
    private String email;

    private boolean lockedFlag = false;

    public boolean isLocked() {
        return lockedFlag;
    }

    public void setLockedFlag(final boolean lockedFlag) {
        this.lockedFlag = lockedFlag;
    }

    @Nullable
    @Override
    public User clone() {
        try {
            return (User) super.clone();
        } catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull val correctLogin = Optional.ofNullable(login).orElse(DEFAULT_LOGIN);
        @NotNull val correctRoleName = role.getRoleName();
        @NotNull val correctFirstName = Optional.ofNullable(firstname).orElse(DEFAULT_FIRSTNAME);
        @NotNull val correctLastName = Optional.ofNullable(lastname).orElse(DEFAULT_LASTNAME);
        @NotNull val correctEmail = Optional.ofNullable(email).orElse(DEFAULT_EMAIL);
        @NotNull val correctUserStatus = (isLocked()) ? LOCKED : ACTIVE;

        return "[ID: " + getId() +
                "; Login: " + correctLogin +
                "; Role: " + correctRoleName +
                "; First Name: " + correctFirstName +
                "; Last Name: " + correctLastName +
                "; Email: " + correctEmail +
                "; Status: " + correctUserStatus + "]";
    }

}
