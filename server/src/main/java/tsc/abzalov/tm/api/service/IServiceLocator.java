package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IApplicationPropertyService getPropertyService();

}
