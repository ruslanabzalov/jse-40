package tsc.abzalov.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

import java.sql.Timestamp;

public interface ISessionRepository {

    @Insert("INSERT INTO session " +
            "(id, open_date, user_id) " +
            "VALUES " +
            "(#{id}, #{openDate}, #{userId});")
    void addSession(@NotNull @Param("id") Long id,
                    @NotNull @Param("openDate") Timestamp openDate,
                    @NotNull @Param("userId") Long userId);

    @Delete("DELETE " +
            "FROM session " +
            "WHERE id = #{id};")
    void removeSession(@NotNull @Param("id") final Long id);

    @Nullable
    @Select("SELECT id, open_date, user_id " +
            "FROM session " +
            "WHERE id = #{id};")
    Session findSession(@NotNull @Param("id") final Long id);

}
