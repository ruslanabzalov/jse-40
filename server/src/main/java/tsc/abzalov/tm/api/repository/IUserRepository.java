package tsc.abzalov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Select("SELECT COUNT(id) " +
            "FROM user;")
    long usersSize();

    @Insert("INSERT INTO user " +
            "(id, login, password, role, firstname, lastname, email, locked_flag) " +
            "VALUES " +
            "(#{id}, #{login}, #{password}, #{role}, #{firstname}, #{lastname}, #{email}, #{lockedFlag});")
    void createUser(@NotNull @Param("id") Long id,
                    @NotNull @Param("login") String login,
                    @NotNull @Param("password") String password,
                    @NotNull @Param("role") String role,
                    @NotNull @Param("firstname") String firstname,
                    @Nullable @Param("lastname") String lastname,
                    @NotNull @Param("email") String email,
                    @Param("lockedFlag") boolean lockedFlag);

    @Delete("DELETE " +
            "FROM user " +
            "WHERE login <> 'admin';")
    void clearAllUsers();

    @NotNull
    @Select("SELECT id, login, password, role, firstname, lastname, email, locked_flag " +
            "FROM user;")
    List<User> findAllUsers();

    @Nullable
    @Select("SELECT id, login, password, role, firstname, lastname, email, locked_flag " +
            "FROM user " +
            "WHERE id = #{id};")
    User findUserById(@NotNull @Param("id") Long id);

    @Nullable
    @Select("SELECT id, login, password, role, firstname, lastname, email, locked_flag " +
            "FROM user " +
            "WHERE login = #{login} " +
            "LIMIT 1;")
    User findUserByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT id, login, password, role, firstname, lastname, email, locked_flag " +
            "FROM user " +
            "WHERE email = #{email} " +
            "LIMIT 1;")
    User findUserByEmail(@NotNull @Param("email") String email);

    @Delete("DELETE FROM user " +
            "WHERE id = #{id};")
    void removeUserById(@NotNull @Param("id") Long id);

    @Delete("DELETE " +
            "FROM user " +
            "WHERE login = #{login};")
    void removeUserByLogin(@NotNull String login);

    @Update("UPDATE user " +
            "SET password = #{password} " +
            "WHERE id = #{id};")
    void editUserPassword(@NotNull @Param("id") Long id,
                          @NotNull @Param("password") String hashedPassword);

    @Update("UPDATE user " +
            "SET firstname = #{firstname}, lastname = #{lastname} " +
            "WHERE id = #{id};")
    void editUserInfo(@NotNull @Param("id") Long id,
                      @NotNull @Param("firstname") String firstName,
                      @Nullable @Param("lastname") String lastName);

    @Update("UPDATE user " +
            "SET locked_flag = #{lockedFlag} " +
            "WHERE id = #{id};")
    void lockUnlockUserById(@NotNull @Param("id") Long id,
                            @Param("lockedFlag") boolean lockedFlag);

    @Update("UPDATE user " +
            "SET locked_flag = #{lockedFlag} " +
            "WHERE login = #{login};")
    void lockUnlockUserByLogin(@NotNull @Param("login") String login,
                               @Param("lockedFlag") boolean lockedFlag);

}
