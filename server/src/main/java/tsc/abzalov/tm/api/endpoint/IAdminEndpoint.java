package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.User;

import java.util.List;

public interface IAdminEndpoint {

    void adminCreateUserWithCustomRole(@Nullable Session session, @Nullable String login,
                                       @Nullable String password, @Nullable Role role,
                                       @Nullable String firstName, @Nullable String lastName,
                                       @Nullable String email);

    void adminDeleteUserByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User adminLockUnlockUserById(@Nullable Session session, @Nullable Long id);

    @Nullable
    User adminLockUnlockUserByLogin(@Nullable Session session, @Nullable String login);

    long adminSizeUsers(@Nullable Session session);

    boolean adminIsEmptyUserList(@Nullable Session session);

    void adminCreateUserWithEntity(@Nullable Session session, @Nullable User user);

    void adminAddAllUsers(@Nullable Session session, @Nullable List<User> users);

    @NotNull
    List<User> adminFindAllUsers(@Nullable Session session);

    @Nullable
    User adminFindUsersById(@Nullable Session session, @Nullable Long id);

    void adminClearAllUsers(@Nullable Session session);

    void adminRemoveUserById(@Nullable Session session, @Nullable Long id);

    void adminCreateUser(@Nullable Session session, @Nullable final String login,
                         @Nullable final String password, @Nullable final String firstName,
                         @Nullable final String lastName, @Nullable final String email);

    boolean adminIsUserExist(@Nullable Session session, @Nullable String login, @Nullable String email);

    @Nullable
    User adminFindUserUserById(@Nullable Session session, @Nullable Long userId);

    @Nullable
    User adminFindUserByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User adminEditPasswordById(@Nullable Session session, @Nullable Long userId, @Nullable String newPassword);

    @Nullable
    User adminEditUserInfoById(@Nullable Session session, @Nullable Long userId,
                               @Nullable String firstName, @Nullable String lastName);

}
