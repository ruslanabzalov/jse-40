package tsc.abzalov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull SqlSession getSession();

}
