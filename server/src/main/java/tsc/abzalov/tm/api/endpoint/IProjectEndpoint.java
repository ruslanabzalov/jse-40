package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Session;

import java.util.List;

public interface IProjectEndpoint {

    long sizeProjects(@Nullable Session session);

    boolean isEmptyProjectList(@Nullable Session session);

    void createProject(@Nullable Session session, @NotNull String name, @NotNull String description);

    void addAllProjects(@Nullable Session session, @Nullable List<Project> projects);

    @NotNull
    List<Project> findAllProjects(@Nullable Session session);

    @Nullable
    Project findProjectsById(@Nullable Session session, @Nullable Long id);

    void clearAllProjects(@Nullable Session session);

    long projectsSize(@Nullable Session session);

    boolean areProjectsEmpty(@Nullable Session session);

    @NotNull
    List<Project> findAllProjectsById(@Nullable Session session);

    @Nullable
    Project findProjectById(@Nullable Session session, @Nullable Long id);

    @Nullable
    Project findProjectByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Project editProjectById(@Nullable Session session, @Nullable Long id,
                            @Nullable String name, @Nullable String description);

    @Nullable
    Project editProjectByName(@Nullable Session session, @Nullable String name,
                              @Nullable String description);

    void clearProjects(@Nullable Session session);

    void removeProjectById(@Nullable Session session, @Nullable Long id);

    void removeProjectByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Project startProjectById(@Nullable Session session, @Nullable Long id);

    @Nullable
    Project endProjectById(@Nullable Session session, @Nullable Long id);

    @NotNull
    List<Project> sortProjectsByName(@Nullable Session session);

    @NotNull
    List<Project> sortProjectsByStartDate(@Nullable Session session);

    @NotNull
    List<Project> sortProjectsByEndDate(@Nullable Session session);

    @NotNull
    List<Project> sortProjectsByStatus(@Nullable Session session);
    
}
