package tsc.abzalov.tm.service;

import lombok.val;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ISessionRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.property.IDatabasePropertyService;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private static final String ENV_ID = "Development";

    @NotNull
    private static final String REPOS_PACKAGE_NAME = "tsc.abzalov.tm.api.repository";

    @NotNull
    private final IDatabasePropertyService propertyService;

    public ConnectionService(@NotNull final IDatabasePropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public SqlSession getSession() {
        @NotNull val url = propertyService.getDatabaseUrl();
        @NotNull val user = propertyService.getDatabaseUser();
        @NotNull val password = propertyService.getDatabasePassword();
        @NotNull val driver = propertyService.getDatabaseDriver();
        @NotNull val datasource = new PooledDataSource(driver, url, user, password);
        @NotNull val transactionFactory = new JdbcTransactionFactory();
        @NotNull val environment = new Environment(ENV_ID, transactionFactory, datasource);

        @NotNull val configuration = new Configuration(environment);
        configuration.addMappers(REPOS_PACKAGE_NAME);
        configuration.setMapUnderscoreToCamelCase(true);
        @NotNull val sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
        return sqlSessionFactory.openSession();
    }

}
