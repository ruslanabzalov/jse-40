package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.User;

import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IHashingPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IHashingPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    public long size() {
        try (@NotNull val session = connectionService.getSession()) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            return userRepository.usersSize();
        }
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final User user) {
        if (user == null) throw new EntityNotFoundException();

        @NotNull val id = user.getId();
        @Nullable val login = user.getLogin();
        @Nullable val password = user.getPassword();
        @NotNull val role = user.getRole().toString();
        @Nullable val firstname = user.getFirstname();
        @Nullable val lastname = user.getLastname();
        @Nullable val email = user.getEmail();
        boolean isLocked = user.isLocked();

        checkMainInfo(login, password, firstname, email);

        @NotNull val session = connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            userRepository.createUser(id, login, password, role, firstname, lastname, email, isLocked);
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, role, firstName, lastName, email));
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, USER, firstName, lastName, email));
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<User> users) {
        if (users == null) throw new EntityNotFoundException();
        for (@NotNull val user : users) this.create(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findAllUsers();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            userRepository.clearAllUsers();
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            userRepository.removeUserById(id);
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            return Optional.ofNullable(userRepository.findUserById(id)).orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @SneakyThrows
    public boolean isUserExist(@Nullable String login, @Nullable String email) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        email = Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);

            val isExistByLogin = userRepository.findUserByLogin(login) != null;
            val isExistByEmail = userRepository.findUserByEmail(email) != null;

            return isExistByLogin && isExistByEmail;
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            @Nullable val searchedUser = userRepository.findUserByLogin(login);
            return Optional.ofNullable(searchedUser).orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editPasswordById(@Nullable Long id, @Nullable String newPassword) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        newPassword = Optional.ofNullable(newPassword).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @Nullable User user = null;
        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            userRepository.editUserPassword(id, hash(newPassword, counter, salt));
            localSession.commit();
            user = userRepository.findUserById(id);
        } catch (Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return Optional.ofNullable(user).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editUserInfoById(@Nullable Long id, @Nullable String firstName, @Nullable final String lastName) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        firstName = Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);

        @Nullable User user = null;
        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            @NotNull val userRepository = session.getMapper(IUserRepository.class);
            userRepository.editUserInfo(id, firstName, lastName);
            localSession.commit();
            user = userRepository.findUserById(id);
        } catch (Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return Optional.ofNullable(user).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void deleteByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val session = this.connectionService.getSession();
        @NotNull val userRepository = session.getMapper(IUserRepository.class);
        try (@NotNull val localSession = session) {
            userRepository.removeUserByLogin(login);
            localSession.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUnlockById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val session = this.connectionService.getSession();
        @NotNull val userRepository = session.getMapper(IUserRepository.class);

        @NotNull val user = Optional
                .ofNullable(userRepository.findUserById(id))
                .orElseThrow(EntityNotFoundException::new);
        boolean neededFlag = user.isLocked();

        @Nullable User changedUser = null;
        try (@NotNull val localSession = session) {
            userRepository.lockUnlockUserById(id, neededFlag);
            localSession.commit();
            changedUser = userRepository.findUserById(id);
        } catch (Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return Optional
                .ofNullable(changedUser)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUnlockByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val session = this.connectionService.getSession();
        @NotNull val userRepository = session.getMapper(IUserRepository.class);

        @NotNull val user = Optional.ofNullable(userRepository.findUserByLogin(login)).orElseThrow(EntityNotFoundException::new);
        boolean neededFlag = !user.isLocked();

        @Nullable User changedUser = null;
        try (@NotNull val localSession = session) {
            userRepository.lockUnlockUserByLogin(login, neededFlag);
            localSession.commit();
            changedUser = userRepository.findUserByLogin(login);
        } catch (Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return Optional
                .ofNullable(changedUser)
                .orElseThrow(EntityNotFoundException::new);
    }

    @SneakyThrows
    private void checkMainInfo(@Nullable String login, @Nullable String password,
                               @Nullable String firstName, @Nullable String email) {
        if (login == null) throw new IncorrectCredentialsException();
        if (password == null) throw new IncorrectCredentialsException();
        if (firstName == null) throw new EmptyFirstNameException();
        if (email == null) throw new EmptyEmailException();
    }

    @NotNull
    @SneakyThrows
    private User createUser(@Nullable String login, @Nullable String password,
                            @Nullable Role role, @Nullable String firstName,
                            @Nullable String lastName, @Nullable String email) {
        checkMainInfo(login, password, firstName, email);

        @NotNull val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @NotNull val user = new User();
        user.setLogin(login);
        user.setPassword(hash(password, counter, salt));
        if (role != null) user.setRole(role);
        else user.setRole(USER);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setEmail(email);
        return user;
    }

}
