package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.exception.system.PropertiesAreNotExistException;

import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static tsc.abzalov.tm.util.LiteralConst.*;

public final class ApplicationPropertyService implements IApplicationPropertyService {

    @NotNull
    private static final String PROPERTIES_FILENAME = "application.properties";

    @NotNull
    private static final Map<String, String> SYS_ENVS = System.getenv();

    @NotNull
    private static final Properties SYS_PROPS = System.getProperties();

    @NotNull
    private final Properties properties = new Properties();

    @Override
    @SneakyThrows
    public void initLocalProperties() {
        @Nullable val appProperties = ClassLoader.getSystemResourceAsStream(PROPERTIES_FILENAME);
        Optional.ofNullable(appProperties).orElseThrow(PropertiesAreNotExistException::new);
        properties.load(appProperties);
    }

    @Override
    @NotNull
    public String getAppVersion() {
        return getStringProperty("application.version", DEFAULT_APP_VERSION);
    }

    @Override
    @NotNull
    public String getDeveloperName() {
        return getStringProperty("application.developer.name", DEFAULT_DEVELOPER_NAME);
    }

    @Override
    @NotNull
    public String getDeveloperEmail() {
        return getStringProperty("application.developer.email", DEFAULT_DEVELOPER_EMAIL);
    }

    @Override
    @NotNull
    public String getPasswordSaltProperty() {
        return getStringProperty("application.password.salt", DEFAULT_PASSWORD_HASHING_SALT);
    }

    @Override
    @NotNull
    public Integer getPasswordCounterProperty() {
        if (SYS_ENVS.containsKey("application.password.counter"))
            return Integer.parseInt(System.getenv("application.password.counter"));

        if (SYS_PROPS.containsKey("application.password.counter"))
            return Integer.parseInt(System.getProperty("application.password.counter", DEFAULT_PASSWORD_HASHING_COUNTER));

        return Integer.parseInt(properties.getProperty("application.password.counter", DEFAULT_PASSWORD_HASHING_COUNTER));
    }

    @Override
    @NotNull
    public String getSessionSaltProperty() {
        return getStringProperty("application.session.salt", DEFAULT_SESSION_HASHING_SALT);
    }

    @Override
    @NotNull
    public Integer getSessionCounterProperty() {
        if (SYS_ENVS.containsKey("application.session.counter"))
            return Integer.parseInt(System.getenv("application.session.counter"));

        if (SYS_PROPS.containsKey("application.session.counter"))
            return Integer.parseInt(System.getProperty("application.session.counter", DEFAULT_SESSION_HASHING_COUNTER));

        return Integer.parseInt(properties.getProperty("application.session.counter", DEFAULT_SESSION_HASHING_COUNTER));
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringProperty("server.host", DEFAULT_SERVER_HOST);
    }

    @Override
    @NotNull
    public String getServerPort() {
        return getStringProperty("server.port", DEFAULT_SERVER_PORT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringProperty("jdbc.url", DEFAULT_DATABASE_URL);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringProperty("jdbc.user", DEFAULT_DATABASE_USER);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringProperty("jdbc.password", DEFAULT_DATABASE_PASSWORD);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringProperty("jdbc.driver", DEFAULT_DATABASE_DRIVER);
    }

    @NotNull
    private String getStringProperty(@NotNull final String key, @NotNull final String defaultValue) {
        if (SYS_ENVS.containsKey(key)) return System.getenv(key);
        if (SYS_PROPS.containsKey(key)) return System.getProperty(key, defaultValue);
        return properties.getProperty(key, defaultValue);
    }

}
