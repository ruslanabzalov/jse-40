package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Task;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Status.DONE;
import static tsc.abzalov.tm.enumeration.Status.IN_PROGRESS;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;

public abstract class AbstractBusinessEntityService<T extends AbstractBusinessEntity>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IConnectionService connectionService;

    public AbstractBusinessEntityService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public long size() {
        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return projectRepository.projectsSize();
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return taskRepository.tasksSize();
            }
        }
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final T entity) {
        if (entity == null) throw new EntityNotFoundException();

        @NotNull val id = entity.getId();
        @Nullable val name = entity.getName();
        @Nullable var description = entity.getDescription();
        @Nullable val startDate = (entity.getStartDate() == null)
                ? null
                : Timestamp.valueOf(entity.getStartDate());
        @Nullable val endDate = (entity.getEndDate() == null)
                ? null
                : Timestamp.valueOf(entity.getEndDate());
        @NotNull val status = entity.getStatus();
        @Nullable val userId = entity.getUserId();

        if (name == null) throw new EmptyNameException();
        if (description == null) description = DEFAULT_DESCRIPTION;
        if (userId == null) throw new EmptyIdException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.createProject(id, name, description, startDate, endDate, status.toString(), userId);
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);

                @NotNull val task = (Task) entity;
                @Nullable val projectId = task.getProjectId();

                taskRepository.createTask(id, name, description, startDate, endDate, status.toString(), userId, projectId);
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<T> entities) {
        if (entities == null) throw new EmptyEntityException();
        entities.forEach(this::create);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<T> findAll() {
        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (List<T>) Optional
                        .ofNullable(projectRepository.findAllProjects())
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (List<T>) Optional
                        .ofNullable(taskRepository.findAllTasks())
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public T findById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (T) Optional
                        .ofNullable(projectRepository.findProjectById(id))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (T) Optional
                        .ofNullable(taskRepository.findTaskById(id))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @Override
    public void clear() {
        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.clearAllProjects();
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.clearAllTasks();
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.removeProjectById(id);
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.removeTaskById(id);
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @SneakyThrows
    public long size(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return projectRepository.projectsSizeByUserId(userId);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return taskRepository.tasksSizeByUserId(userId);
            }
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        return this.size(userId) == 0;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (List<T>) Optional
                        .ofNullable(projectRepository.findProjectsByUserId(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (List<T>) Optional
                        .ofNullable(taskRepository.findTasksByUserId(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public T findByName(@Nullable Long userId, @Nullable String name) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (T) Optional
                        .ofNullable(projectRepository.findProjectByName(userId, name))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (T) Optional
                        .ofNullable(taskRepository.findTaskByName(userId, name))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public T editById(@Nullable final Long id, @Nullable final String name,
                      @Nullable final String description) {
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val correctDescription = (description == null) ? DEFAULT_DESCRIPTION : description;

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.editProjectById(id, name, correctDescription);
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.editTaskById(id, name, correctDescription);
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return this.findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public T editByName(@Nullable final Long userId, @Nullable final String name,
                        @Nullable final String description) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val correctDescription = (description == null) ? DEFAULT_DESCRIPTION : description;

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.editProjectByName(userId, name, correctDescription);
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.editTaskByName(userId, name, correctDescription);
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return this.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final Long userId) {
        if (userId == null) throw new EmptyIdException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.clearProjectByUserId(userId);
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.clearTasksByUserId(userId);
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.removeProjectByName(userId, name);
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.removeTaskByName(userId, name);
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public T startById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.startProjectById(id, Timestamp.valueOf(LocalDateTime.now()), IN_PROGRESS.toString());
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.startTaskById(id, Timestamp.valueOf(LocalDateTime.now()), IN_PROGRESS.toString());
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return this.findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public T endById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val session = this.connectionService.getSession();
        try (@NotNull val localSession = session) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = localSession.getMapper(IProjectRepository.class);
                projectRepository.endProjectById(id, Timestamp.valueOf(LocalDateTime.now()), IN_PROGRESS.toString());
            } else {
                @NotNull val taskRepository = localSession.getMapper(ITaskRepository.class);
                taskRepository.endTaskById(id, Timestamp.valueOf(LocalDateTime.now()), DONE.toString());
            }
            localSession.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            session.rollback();
        }

        return this.findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByName(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByName(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByName(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStartDate(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByStartDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByStartDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByEndDate(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByEndDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByEndDate(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStatus(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        try (@NotNull val session = this.connectionService.getSession()) {
            if (this instanceof ProjectService) {
                @NotNull val projectRepository = session.getMapper(IProjectRepository.class);
                return (List<T>) Optional
                        .ofNullable(projectRepository.sortProjectsByStatus(userId))
                        .orElseThrow(EntityNotFoundException::new);
            } else {
                @NotNull val taskRepository = session.getMapper(ITaskRepository.class);
                return (List<T>) Optional
                        .ofNullable(taskRepository.sortTasksByStatus(userId))
                        .orElseThrow(EntityNotFoundException::new);
            }
        }
    }

}
