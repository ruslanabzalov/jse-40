package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.ILoggerService;
import tsc.abzalov.tm.exception.logger.EmptyCommandException;
import tsc.abzalov.tm.exception.logger.EmptyMessageException;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.*;

import static java.util.logging.Logger.getGlobal;
import static java.util.logging.Logger.getLogger;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String COMMANDS_LOG_FILE_PATH = "logs/access.log";

    @NotNull
    private static final String ERRORS_LOG_FILE_PATH = "logs/errors.log";

    @NotNull
    private final Logger messages = getLogger(MESSAGES);

    @NotNull
    private final Logger commands = getLogger(COMMANDS);

    @NotNull
    private final Logger error = getLogger(ERRORS);

    @NotNull
    private final Logger root = getGlobal();

    public LoggerService() {
        initLogger(messages, null);
        initLogger(commands, COMMANDS_LOG_FILE_PATH);
        initLogger(error, ERRORS_LOG_FILE_PATH);
    }

    @Override
    @SneakyThrows
    public void info(@Nullable String message) {
        message = Optional.ofNullable(message).orElseThrow(EmptyMessageException::new);
        messages.info(message);
    }

    @Override
    @SneakyThrows
    public void command(@Nullable String cmd) {
        cmd = Optional.ofNullable(cmd).orElseThrow(EmptyCommandException::new);
        commands.info("Execution of command \"" + cmd + "\"");
    }

    @Override
    public void error(@NotNull final Exception exception) {
        error.log(Level.SEVERE, exception.getMessage(), exception);
    }

    private void initLogger(@NotNull final Logger logger, @Nullable final String filePath) {
        logger.setUseParentHandlers(false);
        logger.addHandler(getConsoleHandler());

        val isFilePathExist = Optional.ofNullable(filePath).isPresent();
        if (isFilePathExist) {
            try {
                logger.addHandler(new FileHandler(filePath));
            } catch (@NotNull final IOException exception) {
                root.severe(exception.getMessage());
            }
        }
    }

    @NotNull
    private Handler getConsoleHandler() {
        @NotNull val consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new Formatter() {
            @Override
            public String format(@NotNull final LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return consoleHandler;
    }

}
