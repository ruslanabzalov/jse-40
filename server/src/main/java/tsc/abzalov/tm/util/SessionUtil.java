package tsc.abzalov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

import static tsc.abzalov.tm.util.HashUtil.hash;

@UtilityClass
public class SessionUtil {

    @Nullable
    public static Session signSession(@NotNull final Session session, @NotNull final Integer counter,
                                     @NotNull final String salt) throws JsonProcessingException {
        @NotNull val signature = hash(new ObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(session), counter, salt);
        @Nullable val signedSession = session.clone();
        if (signedSession == null) return null;
        signedSession.setSignature(signature);
        return signedSession;
    }

}
