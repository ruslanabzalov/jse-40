package tsc.abzalov.tm.endpoint;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static tsc.abzalov.tm.endpoint.Status.*;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;

class TaskEndpointTest {

    @NotNull
    private static final String LOGIN = "admin";

    @NotNull
    private static final String PASSWORD = "admin";

    @NotNull
    private static final String TASK_NAME = "Testing Task Name";

    @NotNull
    private static final String TASK_DESCRIPTION = "Testing Task Description";

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Nullable
    private Session session;

    @Nullable
    private Task task;

    @BeforeEach
    void setUp() {
        this.session = this.sessionEndpoint.openSession(LOGIN, PASSWORD);
        taskEndpoint.createTask(this.session, TASK_NAME, TASK_DESCRIPTION);
        task = taskEndpoint.findTaskByName(session, TASK_NAME);
    }

    @AfterEach
    void tearDown() {
        taskEndpoint.clearTasks(session);
        if (this.session != null) this.sessionEndpoint.closeSession(session);
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Task By Id Test")
    void editTaskById() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskById(null, this.task.getId(), TASK_NAME, TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskById(this.session, null, TASK_NAME, TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskById(this.session, this.task.getId(), null, TASK_DESCRIPTION)
                ),
                () -> {
                    @Nullable val editedTask =
                            taskEndpoint.editTaskById(this.session, this.task.getId(), TASK_NAME + TASK_NAME, null);
                    assertNotNull(editedTask);
                    assertEquals(TASK_NAME + TASK_NAME, editedTask.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Task Index Test")
    void taskIndex() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.taskIndex(null, this.task)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.taskIndex(this.session, null)
                ),
                () -> {
                    val taskCorrectIndex =
                            taskEndpoint.sizeTasks(session) > 0 ? taskEndpoint.sizeTasks(session) - 1 : 0;
                    val taskIndex = taskEndpoint.taskIndex(this.session, this.task);
                    assertEquals(taskCorrectIndex, taskIndex);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Task By Id Test")
    void findTasksById() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTasksById(null, this.task.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTasksById(this.session, null)
                ),
                () -> {
                    @Nullable val foundedTask =
                            taskEndpoint.findTasksById(this.session, this.task.getId());
                    assertNotNull(foundedTask);
                    assertEquals(this.task.getName(), foundedTask.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find All Tasks Test")
    void findAllTasks() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findAllTasks(null)
                ),
                () -> {
                    @NotNull val tasks = taskEndpoint.findAllTasks(this.session);
                    assertNotNull(tasks);
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Empty Task List Test")
    void isEmptyTaskList() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.isEmptyTaskList(null)
                ),
                () -> {
                    assertFalse(taskEndpoint.isEmptyTaskList(session));
                    taskEndpoint.clearAllTasks(session);
                    assertTrue(taskEndpoint.isEmptyTaskList(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Remove Task By Index Test")
    void removeTaskByIndex() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.removeTaskByIndex(null, 0)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.removeTaskByIndex(this.session, -1)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            val taskIndex = taskEndpoint.taskIndex(this.session, this.task) + 1;
                            taskEndpoint.removeTaskByIndex(this.session, taskIndex);
                            taskEndpoint.findTaskById(this.session, this.task.getId());
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Task By Name Test")
    void findTaskByName() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTaskByName(null, TASK_NAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTaskByName(this.session, null)
                ),
                () -> {
                    @Nullable val foundedTask =
                            taskEndpoint.findTaskByName(this.session, TASK_NAME);
                    assertNotNull(foundedTask);
                    assertEquals(this.task.getDescription(), foundedTask.getDescription());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Task By Name Test")
    void editTaskByName() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskByName(null, TASK_NAME, TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskByName(this.session, null, TASK_DESCRIPTION)
                ),
                () -> {
                    @Nullable val editedTask =
                            taskEndpoint.editTaskByName(this.session, TASK_NAME, null);
                    assertNotNull(editedTask);
                    assertEquals(DEFAULT_DESCRIPTION, editedTask.getDescription());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Clear All Tasks Test")
    void clearAllTasks() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.clearAllTasks(null)
                ),
                () -> {
                    taskEndpoint.clearAllTasks(this.session);
                    assertTrue(taskEndpoint.isEmptyTaskList(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("End Task By Id Test")
    void endTaskById() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.endTaskById(null, this.task.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.endTaskById(this.session, null)
                ),
                () -> {
                    taskEndpoint.startTaskById(this.session, this.task.getId());
                    @Nullable val endedTask =
                            taskEndpoint.endTaskById(this.session, this.task.getId());
                    assertNotNull(endedTask);
                    assertEquals(DONE, endedTask.getStatus());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Add All Tasks Test")
    void addAllTasks() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.addAllTasks(null, new ArrayList<>())
                ),
                () -> {
                    taskEndpoint.addAllTasks(this.session, null);
                    assertTrue(taskEndpoint.isEmptyTaskList(this.session)); // TODO: Обратить внимание.
                },
                () -> {
                    @NotNull val tasks = new ArrayList<Task>();
                    for (int i = 0; i < 10; i++) {
                        @NotNull val task = new Task();
                        task.setUserId(this.session.getUserId());
                        tasks.add(task);
                    }

                    taskEndpoint.addAllTasks(this.session, tasks);
                    assertEquals(tasks.size(), taskEndpoint.tasksSize(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Task By Id Test")
    void findTaskById() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTaskById(null, this.task.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTaskById(this.session, null)
                ),
                () -> {
                    @Nullable val foundedTask =
                            taskEndpoint.findTaskById(this.session, this.task.getId());
                    assertNotNull(foundedTask);
                    assertEquals(this.task.getName(), foundedTask.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Clear Tasks Test")
    void clearTasks() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.clearTasks(null)
                ),
                () -> {
                    taskEndpoint.clearTasks(this.session);
                    assertTrue(taskEndpoint.areTasksEmpty(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Start Task By Id Test")
    void startTaskById() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.startTaskById(null, this.task.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.startTaskById(this.session, null)
                ),
                () -> {
                    @Nullable val startedTask =
                            taskEndpoint.startTaskById(this.session, this.task.getId());
                    assertNotNull(startedTask);
                    assertEquals(IN_PROGRESS, startedTask.getStatus());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Task By Index Test")
    void findTaskByIndex() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTaskByIndex(null, 0)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.findTaskByIndex(this.session, -1)
                ),
                () -> {
                    val taskIndex = taskEndpoint.taskIndex(this.session, this.task) + 1;
                    taskEndpoint.findTaskByIndex(this.session, taskIndex);
                    @NotNull val foundedTask = taskEndpoint.findTaskById(this.session, this.task.getId());
                    assertEquals(this.task.getName(), foundedTask.getName());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Remove Task By Id Test")
    void removeTaskById() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.removeTaskById(null, this.task.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.removeTaskById(this.session, null)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            taskEndpoint.removeTaskById(this.session, this.task.getId());
                            taskEndpoint.findTaskById(this.session, this.task.getId());
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Tasks Size Test")
    void tasksSize() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.tasksSize(null)
                ),
                () -> {
                    @NotNull val tasks = new ArrayList<Task>();
                    for (int i = 0; i < 10; i++) {
                        @NotNull val task = new Task();
                        task.setUserId(this.session.getUserId());
                        tasks.add(task);
                    }

                    taskEndpoint.addAllTasks(this.session, tasks);
                    assertEquals(tasks.size(), taskEndpoint.tasksSize(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Create Task Test")
    void createTask() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.createTask(null, TASK_NAME, TASK_DESCRIPTION)
                ),
                () -> {
                    taskEndpoint.createTask(this.session, TASK_NAME + 1, TASK_DESCRIPTION + 1); // TODO: Обратить внимание.
                    assertEquals(2, taskEndpoint.tasksSize(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Tasks Empty Test")
    void areTasksEmpty() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.areTasksEmpty(null)
                ),
                () -> {
                    assertFalse(taskEndpoint.areTasksEmpty(this.session));
                    taskEndpoint.removeTaskById(this.session, this.task.getId());
                    assertTrue(taskEndpoint.areTasksEmpty(this.session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Remove Task By Name Test")
    void removeTaskByName() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.removeTaskByName(null, TASK_NAME)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.removeTaskByName(this.session, null)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> {
                            taskEndpoint.removeTaskByName(this.session, TASK_NAME);
                            taskEndpoint.findTaskById(this.session, this.task.getId());
                        }
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Edit Task By Index Test")
    void editTaskByIndex() {
        assertNotNull(this.task);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskByIndex(null, 1, TASK_NAME, TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskByIndex(this.session, 0, TASK_NAME, TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.editTaskByIndex(this.session, 1, null, TASK_DESCRIPTION)
                ),
                () -> {
                    @Nullable val editedTask =
                            taskEndpoint.editTaskByIndex(this.session, 1, TASK_NAME + 1, null);
                    assertNotNull(editedTask);
                    assertEquals(DEFAULT_DESCRIPTION, editedTask.getDescription());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Size Tasks")
    void sizeTasks() {
        assertNotNull(this.session);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> taskEndpoint.sizeTasks(null)
                ),
                () -> {
                    @NotNull val tasks = new ArrayList<Task>();
                    for (int i = 0; i < 10; i++) {
                        @NotNull val task = new Task();
                        task.setUserId(this.session.getUserId());
                        tasks.add(task);
                    }

                    taskEndpoint.addAllTasks(this.session, tasks);
                    assertEquals(tasks.size(), taskEndpoint.sizeTasks(this.session));
                }
        );
    }

}