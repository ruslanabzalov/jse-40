package tsc.abzalov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.api.service.ILoggerService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.component.AbstractBackgroundTaskComponent;
import tsc.abzalov.tm.component.BackupComponent;
import tsc.abzalov.tm.component.CommandsFileScannerComponent;
import tsc.abzalov.tm.endpoint.*;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.service.ApplicationPropertyService;
import tsc.abzalov.tm.service.CommandService;
import tsc.abzalov.tm.service.LoggerService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.util.InputUtil.INPUT;
import static tsc.abzalov.tm.util.SystemUtil.getApplicationPid;

@Getter
public final class CommandBootstrap implements IServiceLocator {

    @NotNull
    private final IApplicationPropertyService propertyService = new ApplicationPropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final AbstractBackgroundTaskComponent backupComponent = new BackupComponent(this);

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final FileBackupEndpointService fileBackupEndpointService = new FileBackupEndpointService();

    @NotNull
    private final FileBackupEndpoint fileBackupEndpoint = fileBackupEndpointService.getFileBackupEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Setter
    @Nullable
    private Session session;

    @NotNull
    private final AbstractBackgroundTaskComponent commandsFileScannerComponent =
            new CommandsFileScannerComponent(this);

    {
        registerCommands();
        propertyService.initLocalProperties();
        registerApplicationPid();

    }

    public void run(@NotNull final String... args) {
        System.out.println();
        if (areArgsExecuted(args)) return;

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, \"login\" or \"register\" commands to start working with the application.\n");

        runBackgroundTasks();

        @NotNull val availableStartupCommands =
                Arrays.asList("register", "login", "help", "exit", "info", "about", "exit", "help", "version");
        @Nullable var commandName = "";
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();

            if (isEmpty(commandName)) continue;

            if (session == null) {
                inactiveSessionExecution(availableStartupCommands, commandName);
                continue;
            }

            activeSessionExecution(commandName);
        }
    }

    private void runBackgroundTasks() {
        commandsFileScannerComponent.run();
    }

    private boolean areArgsExecuted(@NotNull final String[] args) {
        try {
            if (areArgExists(args)) return true;
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
            return true;
        }
        return false;
    }

    private void inactiveSessionExecution(@NotNull final List<String> availableStartupCommands,
                                          @NotNull final String commandName) {
        val isCommandAvailable = availableStartupCommands.contains(commandName);

        if (!isCommandAvailable) {
            System.out.println("Session is inactive! Please, register new user or login.");
            return;
        }

        try {
            loggerService.command(commandName);
            @NotNull val command = commandService.getCommandByName(commandName);
            command.execute();
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    private void activeSessionExecution(@NotNull final String commandName) {
        try {
            @NotNull val command = commandService.getCommandByName(commandName);
            @NotNull val commandRoles = command.getRoles();
            @NotNull val user = userEndpoint.findUserById(session);
            @NotNull val currentUserRole = user.getRole();
            val canUserExecuteCommand = commandRoles.contains(currentUserRole);

            if (canUserExecuteCommand) {
                loggerService.command(commandName);
                command.execute();
                return;
            }

            throw new AccessDeniedException();
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    private void registerCommands() {
        commandService.initCommands(this);
    }

    private boolean areArgExists(@NotNull final String... args) {
        if (isEmpty(args)) return false;

        @NotNull val arg = args[0];
        if (isEmpty(arg)) return false;

        @NotNull val command = commandService.getArgumentByName(arg);
        command.execute();
        return true;
    }

    private void registerApplicationPid() {
        @NotNull val pidFileName = "task-manager.pid";
        @NotNull val pidFile = new File(pidFileName);
        @NotNull val pid = Long.toString(getApplicationPid());

        try {
            Files.write(Paths.get(pidFile.toURI()), pid.getBytes());
        } catch (@NotNull final IOException exception) {
            loggerService.error(exception);
        }

        pidFile.deleteOnExit();
    }

}
