package tsc.abzalov.tm;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.bootstrap.CommandBootstrap;

public final class Application {

    public static void main(@NotNull final String... args) {
        @NotNull val commandBootstrap = new CommandBootstrap();
        commandBootstrap.run(args);
    }

}
