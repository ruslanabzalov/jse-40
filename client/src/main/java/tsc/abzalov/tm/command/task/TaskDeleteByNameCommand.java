package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;


public final class TaskDeleteByNameCommand extends AbstractCommand {

    public TaskDeleteByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "delete-task-by-name";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Delete task by name.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("DELETE TASK BY NAME");
        @NotNull val taskEndpoint = getServiceLocator().getTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areEntitiesExist = taskEndpoint.tasksSize(session) != 0;

        if (areEntitiesExist) {
            taskEndpoint.removeTaskByName(session, inputName());
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks are empty.\n");
    }

}
