package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Empty password!");
    }

}
